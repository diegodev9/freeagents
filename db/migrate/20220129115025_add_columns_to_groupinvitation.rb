class AddColumnsToGroupinvitation < ActiveRecord::Migration[6.1]
  def change
    add_column :group_invitations, :user_invited, :integer
    add_reference :group_invitations, :user, foreign_key: true
    add_reference :group_invitations, :group, foreign_key: true
  end
end
