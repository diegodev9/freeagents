class AddActiveToPartners < ActiveRecord::Migration[6.1]
  def change
    add_column :partners, :active, :boolean
  end
end
