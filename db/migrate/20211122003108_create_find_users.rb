class CreateFindUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :find_users do |t|
      t.references :user, null: false, foreign_key: true
      t.references :group, null: false, foreign_key: true
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
