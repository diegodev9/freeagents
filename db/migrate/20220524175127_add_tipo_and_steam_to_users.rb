class AddTipoAndSteamToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :tipo, :string
    add_column :users, :steam, :string
  end
end
