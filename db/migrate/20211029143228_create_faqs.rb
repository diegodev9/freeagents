class CreateFaqs < ActiveRecord::Migration[6.1]
  def change
    create_table :faqs do |t|
      t.string :faq_title
      t.string :faq_content

      t.timestamps
    end
  end
end
