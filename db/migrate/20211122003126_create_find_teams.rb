class CreateFindTeams < ActiveRecord::Migration[6.1]
  def change
    create_table :find_teams do |t|
      t.references :user, null: false, foreign_key: true
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
