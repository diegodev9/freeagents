class AddActivecolumnToBanners < ActiveRecord::Migration[6.1]
  def change
    add_column :banners, :active, :boolean
  end
end
