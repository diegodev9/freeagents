class RemoveItemsFromVideos < ActiveRecord::Migration[6.1]
  def change
    remove_column :videos, :voted_by
    remove_column :videos, :votes
  end
end
