class ChangeColumnGamesOnUsers < ActiveRecord::Migration[6.1]
  def change
    change_column :users, :games, :string
  end
end
