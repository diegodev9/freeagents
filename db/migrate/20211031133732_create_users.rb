class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :nickname
      t.string :address
      t.string :email
      t.string :phone
      t.string :country
      t.string :city
      t.string :state
      t.date :borndate
      t.string :devices
      t.string :games
      t.string :gamerole
      t.string :winrate
      t.string :gameplayed
      t.string :timerange
      t.string :findme
      t.string :contacme
      t.belongs_to :role, null: false, foreign_key: true

      t.timestamps
    end
  end
end
