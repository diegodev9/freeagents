class AddLiquipediaToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :liquipedia, :string
  end
end
