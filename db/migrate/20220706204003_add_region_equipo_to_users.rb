class AddRegionEquipoToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :equipo_opcion, :boolean
    add_column :users, :equipo, :string
    add_column :users, :region, :string
  end
end
