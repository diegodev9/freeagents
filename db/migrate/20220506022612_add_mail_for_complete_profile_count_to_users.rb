class AddMailForCompleteProfileCountToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :mail_for_complete_profile_count, :integer
  end
end
