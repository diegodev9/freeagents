class RestorePreviousUser < ActiveRecord::Migration[6.1]
  def change
    remove_reference :users, :game
    add_column :users, :games, :integer
  end
end
