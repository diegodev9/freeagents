class AddColumnsToFindUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :find_users, :rango_horario, :string
    add_column :find_users, :winrate, :string
    add_column :find_users, :rol, :string
    add_column :find_users, :partidas_jugadas, :string
    add_column :find_users, :posicion, :string
    add_column :find_users, :pais, :integer
    add_column :find_users, :juego, :integer
  end
end
