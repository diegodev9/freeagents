class AddTutorToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :tutor_nombre, :string
    add_column :users, :tutor_dni, :integer
    add_column :users, :tutor_parentezco, :string
  end
end
