class AddMailForCompleteProfileToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :mail_for_complete_profile, :date
  end
end
