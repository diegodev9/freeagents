class AddPlayersToGroup < ActiveRecord::Migration[6.1]
  def change
    add_column :groups, :players, :string
  end
end
