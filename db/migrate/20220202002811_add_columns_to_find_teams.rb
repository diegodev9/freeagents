class AddColumnsToFindTeams < ActiveRecord::Migration[6.1]
  def change
    add_column :find_teams, :rango_horario, :string
    add_column :find_teams, :winrate, :string
    add_column :find_teams, :rol, :string
    add_column :find_teams, :partidas_jugadas, :string
    add_column :find_teams, :posicion, :string
    add_column :find_teams, :pais, :integer
    add_column :find_teams, :juego, :integer
  end
end
