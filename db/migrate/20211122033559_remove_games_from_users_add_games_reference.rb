class RemoveGamesFromUsersAddGamesReference < ActiveRecord::Migration[6.1]
  def change
    remove_column :users, :games
    add_reference :users, :game, foreign_key: true
  end
end
