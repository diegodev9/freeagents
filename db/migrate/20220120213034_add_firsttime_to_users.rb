class AddFirsttimeToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :firsttime, :integer
  end
end
