class ModificarTablasUsuarioAgregarWhatsapp < ActiveRecord::Migration[6.1]
  def change
    remove_column :users, :contacme
    remove_column :users, :findme
    add_column :users, :whatsapp, :string
    add_column :users, :contactme, :boolean
    add_column :users, :twitch, :string
    add_column :users, :discord, :string
    add_column :users, :facebook, :string
    add_column :users, :youtube, :string
    add_column :users, :twitter, :string
    add_column :users, :findme, :boolean
  end
end
