class ModifyVotesOnUsers < ActiveRecord::Migration[6.1]
  def change
    rename_column :users, :votes, :trophy
  end
end
