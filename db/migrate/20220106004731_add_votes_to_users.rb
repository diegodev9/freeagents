class AddVotesToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :votes, :string
  end
end
