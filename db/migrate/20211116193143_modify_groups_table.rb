class ModifyGroupsTable < ActiveRecord::Migration[6.1]
  def change
    remove_column :groups, :games_id
    remove_column :groups, :country_id

    add_column :groups, :country, :string
    add_column :groups, :games, :string
  end
end