class AddTeamsToUser < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :teams, :string
  end
end
