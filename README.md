# README

FreeAgents

Requerimientos:

* Server Debian 9 (recomendado)

* node 16 (por nvm), yarn, ruby 3.0.2 (por rvm), rails 6.1.4.6

* Server corriendo nginx, passenger y postgresql 14

* Base de datos freeagents_production

* Seguir esta guía para montar la app: https://www.phusionpassenger.com/library/walkthroughs/deploy/ruby/ownserver/nginx/oss/stretch/deploy_app.html

* Después de seguir la guía se debe poblar la base de datos de países con 'rails db:seed RAILS_ENV=production'

* Luego se debe ingresar a la web y crear una cuenta para el administrador, seguido de esto se debe ingresar por el servidor para modificar la cuenta y darle rol de admin:
    - ingresar a la consola en produccion por el servidor con "rails c -d production"
    - escribir "User.all" e identificar el id: del usuario creado
    - escribir @user = User.find_by(id: id_del_usuario_encontrado_en_paso_anterior)
    - escribir @user.role_id = 2
    - escribir @user.save!

* Deployment instructions

* ...
