require "application_system_test_case"

class FindUsersTest < ApplicationSystemTestCase
  setup do
    @find_user = find_users(:one)
  end

  test "visiting the index" do
    visit find_users_url
    assert_selector "h1", text: "Find Users"
  end

  test "creating a Find user" do
    visit find_users_url
    click_on "New Find User"

    fill_in "Description", with: @find_user.description
    fill_in "Group", with: @find_user.group_id
    fill_in "Title", with: @find_user.title
    fill_in "User", with: @find_user.user_id
    click_on "Create Find user"

    assert_text "Find user was successfully created"
    click_on "Back"
  end

  test "updating a Find user" do
    visit find_users_url
    click_on "Edit", match: :first

    fill_in "Description", with: @find_user.description
    fill_in "Group", with: @find_user.group_id
    fill_in "Title", with: @find_user.title
    fill_in "User", with: @find_user.user_id
    click_on "Update Find user"

    assert_text "Find user was successfully updated"
    click_on "Back"
  end

  test "destroying a Find user" do
    visit find_users_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Find user was successfully destroyed"
  end
end
