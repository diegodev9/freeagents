require "application_system_test_case"

class FindTeamsTest < ApplicationSystemTestCase
  setup do
    @find_team = find_teams(:one)
  end

  test "visiting the index" do
    visit find_teams_url
    assert_selector "h1", text: "Find Teams"
  end

  test "creating a Find team" do
    visit find_teams_url
    click_on "New Find Team"

    fill_in "Description", with: @find_team.description
    fill_in "Title", with: @find_team.title
    fill_in "User", with: @find_team.user_id
    click_on "Create Find team"

    assert_text "Find team was successfully created"
    click_on "Back"
  end

  test "updating a Find team" do
    visit find_teams_url
    click_on "Edit", match: :first

    fill_in "Description", with: @find_team.description
    fill_in "Title", with: @find_team.title
    fill_in "User", with: @find_team.user_id
    click_on "Update Find team"

    assert_text "Find team was successfully updated"
    click_on "Back"
  end

  test "destroying a Find team" do
    visit find_teams_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Find team was successfully destroyed"
  end
end
