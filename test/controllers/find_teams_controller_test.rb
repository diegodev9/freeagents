require "test_helper"

class FindTeamsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @find_team = find_teams(:one)
  end

  test "should get index" do
    get find_teams_url
    assert_response :success
  end

  test "should get new" do
    get new_find_team_url
    assert_response :success
  end

  test "should create find_team" do
    assert_difference('FindTeam.count') do
      post find_teams_url, params: { find_team: { description: @find_team.description, title: @find_team.title, user_id: @find_team.user_id } }
    end

    assert_redirected_to find_team_url(FindTeam.last)
  end

  test "should show find_team" do
    get find_team_url(@find_team)
    assert_response :success
  end

  test "should get edit" do
    get edit_find_team_url(@find_team)
    assert_response :success
  end

  test "should update find_team" do
    patch find_team_url(@find_team), params: { find_team: { description: @find_team.description, title: @find_team.title, user_id: @find_team.user_id } }
    assert_redirected_to find_team_url(@find_team)
  end

  test "should destroy find_team" do
    assert_difference('FindTeam.count', -1) do
      delete find_team_url(@find_team)
    end

    assert_redirected_to find_teams_url
  end
end
