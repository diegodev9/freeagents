require "test_helper"

class UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:one)
  end

  test "should get index" do
    get users_url
    assert_response :success
  end

  test "should get new" do
    get new_user_url
    assert_response :success
  end

  test "should create user" do
    assert_difference('User.count') do
      post users_url, params: { user: { address: @user.address, borndate: @user.borndate, city: @user.city, contacme: @user.contacme, country: @user.country, devices: @user.devices, email: @user.email, findme: @user.findme, gameplayed: @user.gameplayed, gamerole: @user.gamerole, games: @user.games, name: @user.name, nickname: @user.nickname, phone: @user.phone, role_id: @user.role_id, state: @user.state, timerange: @user.timerange, winrate: @user.winrate } }
    end

    assert_redirected_to user_url(User.last)
  end

  test "should show user" do
    get user_url(@user)
    assert_response :success
  end

  test "should get edit" do
    get edit_user_url(@user)
    assert_response :success
  end

  test "should update user" do
    patch user_url(@user), params: { user: { address: @user.address, borndate: @user.borndate, city: @user.city, contacme: @user.contacme, country: @user.country, devices: @user.devices, email: @user.email, findme: @user.findme, gameplayed: @user.gameplayed, gamerole: @user.gamerole, games: @user.games, name: @user.name, nickname: @user.nickname, phone: @user.phone, role_id: @user.role_id, state: @user.state, timerange: @user.timerange, winrate: @user.winrate } }
    assert_redirected_to user_url(@user)
  end

  test "should destroy user" do
    assert_difference('User.count', -1) do
      delete user_url(@user)
    end

    assert_redirected_to users_url
  end
end
