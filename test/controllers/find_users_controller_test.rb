require "test_helper"

class FindUsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @find_user = find_users(:one)
  end

  test "should get index" do
    get find_users_url
    assert_response :success
  end

  test "should get new" do
    get new_find_user_url
    assert_response :success
  end

  test "should create find_user" do
    assert_difference('FindUser.count') do
      post find_users_url, params: { find_user: { description: @find_user.description, group_id: @find_user.group_id, title: @find_user.title, user_id: @find_user.user_id } }
    end

    assert_redirected_to find_user_url(FindUser.last)
  end

  test "should show find_user" do
    get find_user_url(@find_user)
    assert_response :success
  end

  test "should get edit" do
    get edit_find_user_url(@find_user)
    assert_response :success
  end

  test "should update find_user" do
    patch find_user_url(@find_user), params: { find_user: { description: @find_user.description, group_id: @find_user.group_id, title: @find_user.title, user_id: @find_user.user_id } }
    assert_redirected_to find_user_url(@find_user)
  end

  test "should destroy find_user" do
    assert_difference('FindUser.count', -1) do
      delete find_user_url(@find_user)
    end

    assert_redirected_to find_users_url
  end
end
