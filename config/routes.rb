Rails.application.routes.draw do
  resources :partners
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }

  root to: 'home#index', as: :home

  resources :groups, :countries, :devices, :roles, :games, :users, :videos, :banners
  resources :faqs, except: :show
  resources :abouts, except: :show
  resources :find_teams, except: :show
  resources :find_users, except: :show
  resources :sponsors, only: %i[new create]
  resources :contacts, only: %i[new create]
  resources :group_invitations, only: %i[index show destroy]

  get 'administrator/index'
  post '/mensaje_global', to: 'administrator#mensaje_global', as: :mensaje_global
  # get '/sponsors', to: 'home#sponsors'
  get '/ggwp', to: 'home#ggwp'
  get '/terms_conditions', to: 'home#terms_conditions'
  put '/video/:id/vote', to: 'videos#vote', as: 'vote'
  put '/home/:id/firsttime', to: 'home#firsttimeuser', as: 'firsttimeuser'
  get 'contacts/new'
  get 'sponsors/new'
  get '/sponsors', to: 'sponsors#new'
  get '/list_equipos', to: 'home#list_user_equipos'
  get '/ggwp_all', to: 'home#ggwp_all'
  put '/group_invitations', to: 'group_invitations#new_invitation', as: 'new_invitation'

  resources :users do
    get :description
    get :avatar
    get :authorization
  end

  resources :groups do
    get :avatar
    get :new_member
    post :new_member
  end

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
