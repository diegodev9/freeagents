const { environment } = require('@rails/webpacker')
    const webpack = require('webpack')
    environment.plugins.append('Provide',
        new webpack.ProvidePlugin({
            $: '/vendor/assets/bower_components/jquery/dist/jquery.js',
            jQuery: '/vendor/assets/bower_components/jquery/dist/jquery.js',
            Popper: ['popper.js', 'default'],
            Tether: ['/vendor/assets/bower_components/tether/dist/js/tether.js']
        })
    )
module.exports = environment
