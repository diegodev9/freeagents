json.extract! faq, :id, :faq_title, :faq_content, :created_at, :updated_at
json.url faq_url(faq, format: :json)
