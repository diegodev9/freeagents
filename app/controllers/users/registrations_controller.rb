# frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController
  # before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_breadcrumb
  before_action :set_selection_list, only: [:new, :create, :edit, :update]
  layout('application')

  # GET /resource/sign_up
  def new
    @breadcrumbs_subtitle = 'Nuevo'

    super
  end

  # POST /resource
  def create
    super
    borndate = params[:user][:borndate].to_time
    check_borndate = ((Time.zone.now - borndate) / 1.year.seconds).floor < 18 ? 'menor' : 'mayor'

    if check_borndate == 'menor'
      @contact = Contact.new(
        nombre: params[:user][:nickname],
        email: params[:user][:email],
        nickname: '',
        mail_to: 'info@freeagents.gg',
        asunto: 'Nuevo usuario menor de edad',
        mensaje: 'Un usuario menor de edad se registró y está a la espera de la aprobación de su cuenta.',
        opcion: 'menor de edad'
      )
      @contact.request = request
      @contact.deliver
    end
  end

  # GET /resource/edit
  def edit
    @breadcrumbs_subtitle = 'Cambiar contraseña / email'
    super
  end

  # PUT /resource
  def update
    super
  end

  # DELETE /resource
  def destroy
    super
  end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  def cancel
    super
  end

  def set_selection_list
    @devices_list = Device.all.map {|device| [device.name, device.id]}.sort
    @country_list = Country.all.map {|country| [country.name, country.id]}.sort
    @games_list = Game.all.map {|game| [game.name, game.id]}.sort
  end

  protected
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up) {|u| u.permit(:whatsapp, :equipo_opcion, :equipo, :region, :liga, :name, :nickname, :borndate, :timerange, :gamerole, :winrate, :gameplayed, :whatsapp, :phone, :country, :state, :city, :address, :contactme, :findme, :password, :password_confirmation, :devices, :games, :email, :option, :liquipedia, :tipo, :steam, :tutor_nombre, :tutor_dni, :tutor_parentezco, :menor, devices: [], games: [])}
    devise_parameter_sanitizer.permit(:account_update) {|u| u.permit(:name, :nickname, :borndate, :timerange, :gamerole, :winrate, :gameplayed, :whatsapp, :phone, :country, :state, :city, :address, :contactme, :findme, :current_password, :password, :password_confirmation, :email, :liquipedia, :tipo, :steam, devices: [], games: [])}
  end

  def set_breadcrumb
    @breadcrumbs = 'Usuario'
    @games_active = 'active'
  end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_up_params
  #   devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute])
  # end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  def after_sign_up_path_for(user)
    home_path
  #   super(resource)
  end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
end
