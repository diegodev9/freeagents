class BannersController < ApplicationController
  before_action :authenticate_user!, except: :index
  before_action :check_admin
  before_action :set_banner, only: [:show, :edit, :update, :destroy]
  before_action :set_breadcrumb
  layout('application')

  # GET /banners
  def index
    @banners = Banner.all
    if @banners.present?
      @breadcrumbs_subtitle = 'Listado de Banners'
      @banners_pagination = banner.paginate(page: params[:page], per_page: 6).order(name: :asc)
    else
      redirect_to home_path, notice: 'no tienes banners cargados'
    end
  end

  # GET /banners/1
  def show
  end

  # GET /banners/new
  def new
    @breadcrumbs_subtitle = 'Nuevo'
    @banner = Banner.new
  end

  # GET /banners/1/edit
  def edit
  end

  # POST /banners
  def create
    @banner = Banner.new(banner_params)

    if @banner.save
      redirect_to @banner, notice: 'El grupo fué creado.'
    else
      render :new
    end
  end

  # PATCH/PUT /banners/1
  def update
    if @banner.update(banner_params)
      redirect_to @banner, notice: 'El grupo fué actualizado.'
    else
      render :edit
    end
  end

  # DELETE /banners/1
  def destroy
    @banner.image.purge
    @banner.thumb.purge
    @banner.destroy
    redirect_to banners_url, notice: 'El grupo fué eliminado.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_banner
    @banner = Banner.find(params[:id])
  end

  def set_breadcrumb
    @breadcrumbs = 'Banners'
  end

  def check_admin
    redirect_to home_path, notice: 'No estas autorizado para hacer esto' unless current_user.role.id == 2
  end

  # Only allow a list of trusted parameters through.
  def banner_params
    params.require(:banner).permit(:title, :content, :active, :image, :thumb)
  end
end
