class FaqsController < ApplicationController
  before_action :set_faq, only: %i[ show edit update destroy ]
  before_action :set_breadcrumb
  before_action :check_admin, except: :index
  layout('application')

  # GET /faqs or /faqs.json
  def index
    @breadcrumbs_subtitle = 'Preguntas frecuentes'
    @breadcrumbs_new = new_faq_path
    @faqs = Faq.all.order(created_at: :asc)

    if user_signed_in? && current_user.role.id == 2
      @button = true
    end
  end

  # GET /faqs/1 or /faqs/1.json
  def show
  end

  # GET /faqs/new
  def new
    @breadcrumbs_subtitle = 'Nuevo faq'
    @faq = Faq.new
  end

  # GET /faqs/1/edit
  def edit
    @breadcrumbs_subtitle = 'Modificar faq'
  end

  # POST /faqs or /faqs.json
  def create
    @faq = Faq.new(faq_params)

    respond_to do |format|
      if @faq.save
        format.html { redirect_to faqs_url, notice: "Pregunta frecuente creada." }
        format.json { render :show, status: :created, location: @faq }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @faq.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /faqs/1 or /faqs/1.json
  def update
    respond_to do |format|
      if @faq.update(faq_params)
        format.html { redirect_to faqs_url, notice: "Pregunta frecuente modificada." }
        format.json { render :show, status: :ok, location: @faq }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @faq.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /faqs/1 or /faqs/1.json
  def destroy
    @faq.destroy
    respond_to do |format|
      format.html { redirect_to faqs_url, notice: "Pregunta frecuente eliminada." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_faq
      @faq = Faq.find(params[:id])
      @button = false
    end

    def check_admin
      def check_admin
        redirect_to home_path, notice: 'No estas autorizado para hacer esto' unless current_user.role.id == 2
      end
    end

    def set_breadcrumb
      @breadcrumbs = 'faqs'
      @free_agents_active = 'active'
    end

    # Only allow a list of trusted parameters through.
    def faq_params
      params.require(:faq).permit(:faq_title, :faq_content)
    end
end
