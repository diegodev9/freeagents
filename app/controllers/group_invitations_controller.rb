class GroupInvitationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_breadcrumb
  before_action :set_group_invitation, only: [:show, :edit, :update, :destroy]
  before_action :check_user, only: [:show, :destroy]
  layout('application')

  # GET /group_invitations
  def index
    if params[:user_id].present? || params[:group_id].present?
      @user = User.find_by(id: params[:user_id])
      @group = Group.find_by(id: params[:group_id])
      if @user.teams.nil?
        @user.teams = [@group.id.to_s]
        @user.save
        if @group.players.nil?
          @add_player_to_group = Group.find_by(id: @group.id)
          @add_player_to_group.players = [@user.id]
        else
          @add_player_to_group = Group.find_by(id: @group.id)
          @group_players = @group.players.scan(/\d*\d/)
          @add_player_to_group.players = @group_players << @user.id.to_s
        end
        @add_player_to_group.save
        GroupInvitation.find_by(id: params[:invite_id]).destroy
        redirect_to group_invitations_path
      else
        @user_teams = @user.teams.scan(/\d*\d/)
        if @user_teams.include?(@group.id.to_s)
          redirect_to group_invitations_path, notice: "El jugador ya está incluido en el grupo"
        else
          @user.groups = @user_teams << @group.id.to_s unless @user_teams.include?(@group.id.to_s)
          @user.save
          if @group.players.nil?
            @add_player_to_group = Group.find_by(id: @group.id)
            @add_player_to_group.players = [@user.id]
          else
            @add_player_to_group = Group.find_by(id: @group.id)
            @group_players = @group.players.scan(/\d*\d/)
            @add_player_to_group.players = @group_players << @user.id.to_s
          end
          @add_player_to_group.save
          GroupInvitation.find_by(id: params[:invite_id]).destroy
          redirect_to group_invitations_path
        end
      end
    end

    @group_invitations = GroupInvitation.where(user_id: current_user.id, user_invited: 1)
    @group_invitations_pagination = @group_invitations.paginate(page: params[:page], per_page: 6)
  end

  # GET /group_invitations/1
  def show
  end

  # GET /group_invitations/new
  def new
    if current_user.option == 'equipo' && params[:group_id].present? && params[:user_id].present?
      @group_invitation = GroupInvitation.new
    else
      redirect_to home_url, alert: 'No estas autorizado para realizar ésta acción'
    end
  end

  # GET /group_invitations/1/edit
  def edit
    redirect_to home_url, alert: 'No estas autorizado para realizar ésta acción'
  end

  # POST /group_invitations
  def create
    @group_invitation = GroupInvitation.new(group_invitation_params)

    if @group_invitation.save
      redirect_to @group_invitation, notice: 'Invitación enviada.'
    else
      render :new
    end
  end

  # PATCH/PUT /group_invitations/1
  def update
    redirect_to home_url, alert: 'No estas autorizado para realizar ésta acción'
    #if @group_invitation.update(group_invitation_params)
    # redirect_to @group_invitation, notice: 'Group invitation was successfully updated.'
    #else
    # render :edit
    #end
  end

  def new_invitation
    GroupInvitation.create(
      group_id: params[:group_id],
      user_id: params[:user_id],
      user_invited: 1
    )
  end

  # DELETE /group_invitations/1
  def destroy
    @group_invitation.destroy
    redirect_to group_invitations_url, notice: 'Group invitation was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_group_invitation
      @group_invitation = GroupInvitation.find(params[:id])
    end

    def set_breadcrumb
      @breadcrumbs = 'Equipos'
      @breadcrumbs_subtitle = 'Mis invitaciones'
    end

    def check_user
      if @group_invitation.user_id == current_user.id
      else
        redirect_to home_url, alert: 'No estas autorizado para realizar ésta acción'
      end
    end

    # Only allow a list of trusted parameters through.
    def group_invitation_params
      params.require(:group_invitation).permit(:user_invited, :user_id, :group_id)
    end
end
