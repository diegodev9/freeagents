class AdministratorController < ApplicationController
  before_action :authenticate_user!
  before_action :check_admin

  def index
    if params[:video].present?
      @video = Video.find_by(id: params[:video])
      @video.approved = true
      @video.save!
      redirect_to administrator_index_path(home: 'home')
    end
    if params[:users].present?
      @users_pagination = User.paginate(page: params[:page], per_page: 50).order(nickname: :asc)
      # @users = User.all.order(nickname: :asc)
    end
    if params[:inhabilitados].present?
      users = User.where(active: [nil, false])
      @users_pagination = users.paginate(page: params[:page], per_page: 50).order(nickname: :asc)
    end
    if params[:nombre].present?
      users = User.where(User.arel_table[:name].matches("%#{params[:nombre]}%"))
      @users_pagination = users.paginate(page: params[:page], per_page: 50).order(name: :asc)
    end
    if params[:about].present?
      @breadcrumbs_new = new_about_path
      @abouts = About.all
    end
    if params[:faqs].present?
      @breadcrumbs_new = new_faq_path
      @faqs = Faq.all
    end
    if params[:groups].present?
      users = User.where(option: 'equipo')
      @groups_pagination = users.paginate(page: params[:page], per_page: 50).order(nickname: :asc)
      # @groups = Group.all.order(name: :asc)
    end
    if params[:banners].present?
      @breadcrumbs_new = new_banner_path
      @banners = Banner.all
    end
    if params[:partners].present?
      @breadcrumbs_new = new_partner_path
      @partners = Partner.all
    end
    if params[:games].present?
      @breadcrumbs_new = new_game_path
      @games_pagination = Game.paginate(page: params[:page], per_page: 50).order(name: :asc)
      # @games = Game.all
    end
    if params[:devices].present?
      @breadcrumbs_new = new_device_path
      @devices = Device.all
    end
    if params[:countries].present?
      @breadcrumbs_new = new_country_path
      @countries_pagination = Country.paginate(page: params[:page], per_page: 50).order(name: :asc)
      # @countries = Country.all
    end
    if params[:disable_user].present?
      @user = User.find_by(id: params[:disable_user].to_i)
      @user.update(active: false)
      @users_pagination = User.paginate(page: params[:page], per_page: 50).order(nickname: :asc)
    end
    if params[:enable_user].present?
      @user = User.find_by(id: params[:enable_user].to_i)
      @user.update(active: true)
      @users_pagination = User.paginate(page: params[:page], per_page: 50).order(nickname: :asc)
    end
    #@user = User.find_by(id: params[:authorization]) if params[:authorization].present?
    if params[:mensaje].present? && params[:mensaje].present? == 'global'
    end

    if params[:por_fecha].present?
      @users_pagination = User.paginate(page: params[:page], per_page: 50).order(created_at: :desc)
    end

    @videos = Video.all
    @videos_for_check = Video.where(approved: false)
    @users_innactive = User.where(active: [nil, false])
    @users_for_check = @users_innactive
  end

  def mensaje_global
    mensaje = params[:mensaje]
    destinos = if params[:destino] == 'jugadores'
                 destino = User.where(option: 'jugador').map { |jugador| jugador.email }
                 destinatario = "#{destino.count} Jugadores"
                 destino
               elsif params[:destino] == 'equipos'
                 destino = User.where(option: 'equipo', tipo: 'amateur').map { |jugador| jugador.email }
                 destinatario = "#{destino.count} Equipos"
                 destino
               elsif params[:destino] == 'pro'
                 destino = User.where(option: 'equipo', tipo: 'pro').map { |jugador| jugador.email }
                 destinatario = "#{destino.count} Equipos Pro"
                 destino
               else
                 destino = User.all.map { |jugador| jugador.email }
                 destinatario = "#{destino.count} personas"
                 destino
               end

    destinos.each do |destino|
      @contact = Contact.new(
        nombre: 'FreeAgents',
        email: 'info@freeagents.gg',
        mail_to: destino,
        asunto: 'Mensaje de FreeAgents',
        mensaje: mensaje,
        opcion: 'global'
      )
      @contact.request = request
      @contact.deliver
    end
    redirect_to administrator_index_path(mensaje: 'global'), notice: "Mensaje enviado a #{destinatario}"
  end

  private

  def check_admin
    redirect_to home_path unless current_user.role.id == 2
  end
end
