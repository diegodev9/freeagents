class ContactsController < ApplicationController
  before_action :set_breadcrumb

  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(
      nombre: current_user.nickname,
      email: current_user.email,
      nickname: '',
      mail_to: contact_params[:usuario],
      asunto: 'Formulario de búsqueda de FreeAgents',
      mensaje: params[:contact][:mensaje],
      opcion: 'busqueda'
    )
    @contact.request = request
    if @contact.deliver
      flash.now[:error] = nil
      redirect_to home_path, notice: 'El mensaje fué enviado'
    else
      flash.now[:error] = 'El mensaje no se pudo enviar'
      render :new
    end
  end

  def set_breadcrumb
    @breadcrumbs = 'Búsquedas'
    @breadcrumbs_subtitle = 'Aplicar a búsqueda'
  end

  private

  def contact_params
    params.require(:contact).permit(:nombre, :email, :nickname, :usuario, :mail_to, :asunto, :mensaje, :opcion)
  end
end
