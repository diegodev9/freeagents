class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :set_breadcrumb
  before_action :set_selection_list, only: [:new, :edit, :create, :update]
  before_action :set_current_user, only: [:avatar, :description]
  layout('application')

  # GET /users
  def index
    if params[:equipos].present?
      @breadcrumbs = 'Equipos'
      @breadcrumbs_subtitle = 'Listado de Equipos'

      if params[:tipo] == 'pro'
        users = User.where(active: true, option: 'equipo', tipo: 'pro').order(nickname: :asc)
      elsif params[:tipo] == 'amateur'
        users = User.where(active: true, option: 'equipo').and(User.where.not(tipo: 'pro')).order(nickname: :asc)
      end

      @country_options = Country.all.map { |country| [country.name, country.id] }
      @game_options = Game.all.map { |game| [game.name, game.id] }.sort
      if params[:nombre].present?
        @breadcrumbs_subtitle = 'Resultados Búsqueda de Equipos'
        users = User.where(User.arel_table[:nickname].matches('%' + params[:nombre] + '%')).and(User.where(option: 'equipo')).order(nickname: :asc)
      elsif params[:filter].present?
        @breadcrumbs_subtitle = 'Resultados del filtro'
        if params[:tipo] == 'pro'
          find_users_all = User.where(active: true, option: 'equipo', tipo: 'pro')
        else
          find_users_all = User.where(active: true, option: 'equipo', tipo: 'amateur')
        end

        params_select = params.select { |k, v| [k.to_sym, v] if v.present? && k != 'controller' && k != 'filter' && k != 'action' && k != 'commit' }
        search_params = params_select.to_enum.each { |param| ["#{param.first} LIKE ?", param.last] }
        users = find_users_all.where(search_params.permit(:nombre, :liga, :country))
        if params[:gameplayed].present?
          users = users.map { |user| user if user.games.present? && user.games.scan(/\d*\d/).include?(params[:gameplayed]) }.compact
        end
      end
    else
      @breadcrumbs_subtitle = 'Jugadores'
      users = User.where(active: true, option: 'jugador').order(nickname: :asc)
    end
    @users_pagination = users.paginate(page: params[:page], per_page: 18)
  end

  # GET /users/1
  def show
    if @user.role_id == 2 && current_user.role_id != 2
      redirect_to home_path, notice: 'No puedes ver este perfil'
    end

    if @user.option == 'equipo'
      @groups = Group.where(user_id: @user.id)
    end

    @user_own_group = Group.all.where(user_id: @user.id)
    @breadcrumbs_subtitle = 'Perfil de usuario'
    @user_games = @user.games.scan(/\d*\d/) unless @user.games.nil?
    @user_devices = @user.devices.scan(/\d*\d/) unless @user.devices.nil?
    @user_teams = @user.teams.scan(/\d*\d/) unless @user.teams.nil?
    @user_groups = @user.groups unless @user.groups.nil?
    @videos = Video.where(user_id: @user)

    @user_age = ((Time.zone.now - @user.borndate.to_time) / 1.year.seconds).floor
  end

  # GET /users/new
  def new
    @breadcrumbs_subtitle = 'Nuevo usuario'
    @user = User.new
  end

  # GET /users/1/edit
  def edit
    redirect_to user_path(current_user.id), notice: 'No estas autorizado para hacer esto' unless @user.id == current_user.id || current_user.role.id == 2

    if @user.option == 'jugador'
      @breadcrumbs_subtitle = 'Modificar usuario'
      @is_user = true
    else
      @breadcrumbs_subtitle = 'Modificar equipo'
      @is_user = false
    end

    @user_games = @user.games.scan(/\d*\d/) unless @user.games.nil?
    @user_devices = @user.devices.scan(/\d*\d/) unless @user.devices.nil?
  end

  # POST /users
  def create
    @user = User.new(user_params)
    @user.role_id = 1
    user_age = ((Time.zone.now - @user.borndate.to_time) / 1.year.seconds).floor
    if user_age < 18
      @user.active = false
      @user.menor = true
    else
      @user.active = true
      @user.menor = false
    end
    if @user.save
      redirect_to @user, notice: 'Nuevo usuario guardado.'
    else
      render new_user_registration_path
    end

  end

  # PATCH/PUT /users/1
  def update
    if @user.option == 'equipo' && user_params[:tipo] == 'pro' && current_user.role.id != 2
      user_params[:tipo] == 'amateur'
    end

    if @user.update(user_params)
      redirect_to @user, notice: 'Usuario actualizado.'
    else
      unless params[:user][:image].present?
        render :edit
      end
      redirect_to avatar, notice: 'El archivo no debe exceder 1MB y debe ser una imagen'
    end
  end

  # DELETE /users/1
  def destroy
    redirect_to user_path(current_user.id), notice: 'No estas autorizado para hacer esto' unless @user.id == current_user.id || current_user.role.id == 2
    @user.destroy
    redirect_to administrator_index_path(users: 'all'), notice: 'Usuario eliminado.'
  end

  def description
    @breadcrumbs_subtitle = 'Agregar descripcion'
  end

  def avatar
    @breadcrumbs_subtitle = 'Cambiar avatar'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
    #@user = User.find(params[:id])
    @button = false
  end

  def set_current_user
    @user = User.find_by(id: current_user.id)
  end

  def set_selection_list
    @devices_list = Device.all.map { |device| [device.name, device.id] }.sort
    @country_list = Country.all.map { |country| [country.name, country.id] }.sort
    @games_list = Game.all.map { |game| [game.name, game.id] }.sort
  end

  def set_breadcrumb
    if current_user.option == 'jugador'
      @breadcrumbs = 'Jugadores'
    else
      @breadcrumbs = 'Equipos'
    end
  end

  # Only allow a list of trusted parameters through.
  def user_params
    params.require(:user).permit(
      :name,
      :nickname,
      :address,
      :email,
      :phone,
      :country,
      :city,
      :state,
      :borndate,
      :gamerole,
      :winrate,
      :gameplayed,
      :timerange,
      :findme,
      :contactme,
      :whatsapp,
      :twitch,
      :discord,
      :facebook,
      :youtube,
      :twitter,
      :description,
      :encrypted_password,
      :reset_password_token,
      :reset_password_sent_at,
      :remember_created_at,
      :current_sign_in_at,
      :last_sign_in_at,
      :current_sign_in_ip,
      :last_sign_in_ip,
      :confirmation_toke,
      :confirmed_at,
      :confirmation_sent_at,
      :failed_attempts,
      :unlock_token,
      :locked_at,
      :unconfirmed_email,
      :image,
      :firsttime,
      :option,
      :liquipedia,
      :mail_for_complete_profile,
      :mail_for_complete_profile_count,
      :tipo,
      :steam,
      :tutor_nombre,
      :tutor_dni,
      :tutor_parentezco,
      :menor,
      :liga,
      :region,
      :equipo_opcion,
      :equipo,
      :teams,
      groups: [],
      devices: [],
      games: [],
    )
  end
end

