class SponsorsController < ApplicationController
  before_action :set_breadcrumb

  def new
    @sponsor = Sponsor.new
    @partners = Partner.all
  end

  def create
    @sponsor = Sponsor.new(
      nombre: params[:sponsor][:nombre],
      email: params[:sponsor][:email],
      nickname: '',
      mail_to: 'contacto@freeagents.gg',
      asunto: 'Nuevo mensaje de contacto desde seccion partners',
      mensaje: params[:sponsor][:mensaje],
      opcion: 'partners'
    )

    @sponsor.request = request
    if @sponsor.deliver
      flash.now[:error] = nil
      redirect_to home_path, notice: 'El mensaje fué enviado, pronto estaremos en contacto'
    else
      flash.now[:error] = 'El mensaje no se pudo enviar'
      render :new
    end
  end

  def set_breadcrumb
    @breadcrumbs = 'Partners'
    @breadcrumbs_subtitle = 'Se parte de freeagents'
  end
end
