class VideosController < ApplicationController
  before_action :authenticate_user!
  before_action :set_video, only: [:show, :edit, :update, :destroy, :vote]
  before_action :set_breadcrumb
  layout('application')

  # GET /videos
  def index
    redirect_to ggwp_all_path
    @videos = Video.all
  end

  # GET /videos/1
  def show
    redirect_to ggwp_all_path, notice: 'No puedes realizar esta acción'
  end

  # GET /videos/new
  def new
    @video = Video.new
    @breadcrumbs_subtitle = 'Nuevo video'
  end

  # GET /videos/1/edit
  def edit
    redirect_to user_path(current_user.id), notice: 'No estas autorizado para hacer esto' unless @video.user_id == current_user.id || current_user.role.id == 2
    @breadcrumbs_subtitle = 'Editar video'
  end

  # POST /videos
  def create
    @video = Video.new(video_params)
    @video.user_id = current_user.id

    if @video.save
      @contact = Contact.new(
       nombre: current_user.nickname,
       email: current_user.email,
       nickname: '',
       usuario: 'info@freeagents.gg',
       asunto: 'Nuevo video',
       mensaje: 'Nuevo video pendiente de aprobación'
      )
      @contact.request = request
      @contact.deliver

      redirect_to user_path(current_user.id), notice: 'Nuevo video guardado.'
    else
      render :new
    end
  end

  # PATCH/PUT /videos/1
  def update
    if @video.update(video_params)
      redirect_to user_path(current_user.id), notice: 'Video modificado.'
    else
      render :edit
    end
  end

  # DELETE /videos/1
  def destroy
    redirect_to user_path(current_user.id), notice: 'No estas autorizado para hacer esto' unless @video.user_id == current_user.id || current_user.role.id == 2
    @video.destroy

    if params[:ggwp].present?
      redirect_to ggwp_path, notice: 'Video eliminado.'
    elsif params[:ggwp_all].present?
      redirect_to ggwp_all_path, notice: 'Video eliminado.'
    elsif params[:admin].present?
      redirect_to administrator_index_path(home: 'home')
    else
      redirect_to user_path(current_user.id), notice: 'Video eliminado.'
    end
  end

  def vote
    Vote.create(user_id: current_user.id, video_id: @video.id)
    @user = User.find_by(id: @video.user_id)
    if @user.trophy.nil?
      @user.trophy = 1
    else
      @user.trophy = @user.trophy.to_i + 1
    end
    @user.save
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_video
      @video = Video.find(params[:id])
    end

    def set_breadcrumb
      @breadcrumbs = 'compartir video'
    end

    # Only allow a list of trusted parameters through.
    def video_params
      params.require(:video).permit(:video, :user_id, :approved, :embeded)
    end
end
