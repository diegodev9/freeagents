class FindTeamsController < ApplicationController
  before_action :authenticate_user!, except: :index
  before_action :set_find_team, only: [:show, :edit, :update, :destroy]
  before_action :set_breadcrumb
  before_action :set_options_for_select, only: [:new, :update, :show]
  layout('application')

  # GET /find_teams
  def index
    @breadcrumbs_subtitle = 'Listado de Búsqueda'
    @country_options = Country.all.map {|country| [country.name, country.id]}
    @game_options = Game.all.map { |game| [game.name, game.id] }.sort

    if params[:nombre].nil?
      find_teams = FindTeam.all

      if params[:filter].present?
        find_teams_all = FindTeam.all
        params_select = params.select {|k,v| [k.to_sym,v] if v.present? && k != 'controller' && k != 'filter' && k != 'action' && k != 'commit'}
        search_params = params_select.to_enum.each {|param| ["#{param.first} LIKE ?", param.last]}

        find_teams = find_teams_all.where(search_params.permit(:nombre, :winrate, :partidas_jugadas, :rango_horario, :pais, :juego, :rol, :posicion))
      end
    else
      find_users = User.where(User.arel_table[:nickname].matches('%'+params[:nombre]+'%'))
      find_teams = FindTeam.where(user_id: find_users.ids)
    end

    @find_teams_pagination = find_teams.paginate(page: params[:page], per_page: 6)
  end

  # GET /find_teams/1
  def show
  end

  # GET /find_teams/new
  def new
    @find_team = FindTeam.new
    @breadcrumbs_subtitle = 'Nueva Búsqueda'

    #@check = FindTeam.where(user_id: current_user.id)
    #if @check.present?
    #  redirect_to user_path(current_user.id), notice: "Ya tienes una publicación creada."
    #end
  end

  # GET /find_teams/1/edit
  def edit
    @breadcrumbs_subtitle = 'Modificar Búsqueda'
  end

  # POST /find_teams
  def create
    @find_team = FindTeam.new(find_team_params)
    @find_team.user_id = current_user.id

    if @find_team.save
      redirect_to find_teams_path, notice: 'Búsqueda de equipo creada.'
    else
      render :new
    end
  end

  # PATCH/PUT /find_teams/1
  def update
    if @find_team.update(find_team_params)
      redirect_to find_teams_path, notice: 'Búsqueda de equipo modificada.'
    else
      render :edit
    end
  end

  # DELETE /find_teams/1
  def destroy
    @find_team.destroy
    redirect_to find_teams_url, notice: 'Búsqueda de equipo eliminada.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_find_team
      @find_team = FindTeam.find(params[:id])
    end

    def set_breadcrumb
      @breadcrumbs = 'Jugadores buscando equipos'
      @find_active = 'active'
    end

    def set_options_for_select
      @games = Game.all.map { |game| [game.name, game.id] }
      @countries = Country.all.map { |country| [country.name, country.id] }
    end

    # Only allow a list of trusted parameters through.
    def find_team_params
      params.require(:find_team).permit(:user_id, :title, :description, :rango_horario, :winrate, :rol, :partidas_jugadas, :posicion, :pais, :juego)
    end
end