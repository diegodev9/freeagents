class GroupsController < ApplicationController
  before_action :authenticate_user!, except: :index
  before_action :set_group, only: [:show, :edit, :update, :destroy]
  before_action :set_breadcrumb
  before_action :set_selection_list, only: [:new, :edit, :create, :update]
  layout('application')

  # GET /groups
  def index
    @country_options = Country.all.map {|country| [country.name, country.id]}
    @game_options = Game.all.map { |game| [game.name, game.id] }.sort
    @breadcrumbs_subtitle = 'Listado de Equipos'
    @league_options = Group.all.collect { |group| group.league }.uniq

    if params[:nombre].nil?
      groups = Group.all

      if params[:filter].present?
        find_teams_all = Group.all
        params_select = params.select {|k,v| [k.to_sym,v] if v.present? && k != 'controller' && k != 'filter' && k != 'action' && k != 'commit'}
        search_params = params_select.to_enum.each {|param| ["#{param.first} LIKE ?", param.last]}

        groups = find_teams_all.where(search_params.permit!)
      end
    else
      find_groups = Group.where(Group.arel_table[:name].matches('%'+params[:nombre]+'%'))
      groups = Group.where(id: find_groups.ids)
    end
=begin
    if params[:filter].present?
      case params[:filter]
      when "pais"
        @groups = Group.where(country: params[:country])
      when "juego"
        @groups = Group.all.filter do |group|
          if group.games.present? && group.games.scan(/\d*\d/).include?(params[:game])
            group
          end
        end
      when "liga"
        @groups = Group.where(league: params[:liga]).sort
      end
    else
      @groups = Group.all
    end
=end

    if params[:user].present?
      groups = Group.where(user_id: params[:user])
      @breadcrumbs_subtitle = 'Mis Equipos'
    end

    @groups_pagination = groups.paginate(page: params[:page], per_page: 24)
  end

  # GET /groups/1
  def show
    @group_games = @group.games.scan(/\d*\d/) unless @group.games.nil?
    @group_players = @group.players.scan(/\d*\d/) unless @group.players.nil?

    if params[:remove_player].present?
      @user = User.find_by(id: params[:remove_player])
      @user_teams = @user.teams.scan(/\d*\d/).delete(@group.id)
      @user.teams = @user_teams
      @user.save
      @group_players = @group.players.scan(/\d*\d/)
      @group_players.delete(@user.id.to_s)
      @group.players = @group_players
      @group.save
      redirect_to group_path(@group), notice: "Jugador expulsado"
    end
  end

  # GET /groups/new
  def new
    @breadcrumbs_subtitle = 'Nuevo'
    @user = User.find_by id: params[:user]

    if @user.option == 'equipo'
      @group = Group.new
    else
      redirect_to user_path(current_user.id), notice: 'No puedes crear otro grupo'
    end

  end

  # GET /groups/1/edit
  def edit
    redirect_to user_path(current_user.id), notice: 'No estas autorizado para hacer esto' unless @group.user_id == current_user.id || current_user.role.id == 2
    @group_games = @group.games.scan(/\d*\d/) unless @group.games.nil?
  end

  # POST /groups
  def create
    @group = Group.new(group_params)
    @group.user_id = current_user.id
    @user = User.find_by(id: current_user.id)

    if @group.save
      if @user.groups.nil?
        @user.groups = [params[@group.id]]
      else
        #@user_groups = @user.groups.scan(/\d*\d/)
        @user_groups = @user.groups.ids
        #@user.groups = @user_groups << params[@group.id]
      end
      @user.save!
      redirect_to @group, notice: 'El grupo fué creado.'
    else
      render :new
    end
  end

  # PATCH/PUT /groups/1
  def update
    @group.image.purge

    if @group.update(group_params)
      redirect_to @group, notice: 'El grupo fué actualizado.'
    else
      unless params[:group][:image].present?
        render :edit
      end
      redirect_to avatar, notice: 'El archivo no debe exceder 1MB y debe ser una imagen'
    end
  end

  # DELETE /groups/1
  def destroy
    redirect_to user_path(current_user.id), notice: 'No estas autorizado para hacer esto' unless @group.user_id == current_user.id || current_user.role.id == 2
    @users = User.all
    @users.each do |user|
      @user_teams = user.teams.scan(/\d*\d/) - [params[:id]] unless user.teams.nil?
      user.teams = @user_teams
      user.save
    end
    @group.destroy
    if current_user.role.id == 2
      redirect_to groups_url, notice: 'El grupo fué eliminado.'
    else
      redirect_to user_path(current_user), notice: 'El grupo fué eliminado.'
    end
  end

  def avatar
    @breadcrumbs_subtitle = 'Cambiar avatar'
    @group = Group.find_by(id: params[:group_id])
  end

  def new_member
    @group = Group.find_by(id: params[:group_id])
    @breadcrumbs_subtitle = 'Nuevo miembro'
    @users = User.all.map { |user| [user.name, user.id] unless user.role_id == 2}.compact

    if params["/groups/find_user/new_member"].present? || params[:find_user].present?
      if params["/groups/find_user/new_member"].present?
        @find_user = params['/groups/find_user/new_member'][:user]
      else
        @find_user = params[:find_user]
      end
      @users = User.where("nickname like ?", "%" + @find_user + "%").where.not(role_id: 2)

      if params[:user_id].present?
        @user = User.find_by(id: params[:user_id])
        if @user.groups.nil?
          @user.groups = [@group.id.to_s]
          @user.save
          if @group.players.nil?
            @add_player_to_group = Group.find_by(id: @group.id)
            @add_player_to_group.players = [@user.id]
          else
            @add_player_to_group = Group.find_by(id: @group.id)
            @group_players = @group.players.scan(/\d*\d/)
            @add_player_to_group.players = @group_players << @user.id.to_s
          end
          @add_player_to_group.save
          redirect_to group_path(@group.id)
        else
          @user_groups = @user.groups.scan(/\d*\d/)
          if @user_groups.include?(@group.id.to_s)
            redirect_to group_new_member_path(@group.id), notice: "El jugador ya está incluido en el grupo"
          else
            @user.groups = @user_groups << @group.id.to_s unless @user_groups.include?(@group.id.to_s)
            @user.save
            if @group.players.nil?
              @add_player_to_group = Group.find_by(id: @group.id)
              @add_player_to_group.players = [@user.id]
            else
              @add_player_to_group = Group.find_by(id: @group.id)
              @group_players = @group.players.scan(/\d*\d/)
              @add_player_to_group.players = @group_players << @user.id.to_s
            end
            @add_player_to_group.save
            redirect_to group_path(@group.id)
          end
        end
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_group
    @group = Group.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    redirect_to home_path, notice: 'El grupo que buscas no existe'
  end

  def set_selection_list
    @country_list = Country.all.map { |country| [country.name, country.id] }.sort
    @games_list = Game.all.map { |game| [game.name, game.id] }.sort
  end

  def set_breadcrumb
    @breadcrumbs = 'Equipos'
    @equipos_active = 'active'
  end

  # Only allow a list of trusted parameters through.
  def group_params
    params.require(:group).permit(:name, :country, :league, :user_id, :image, games: [], players: [])
  end
end
