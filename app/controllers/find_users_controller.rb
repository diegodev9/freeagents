class FindUsersController < ApplicationController
  before_action :authenticate_user!, except: :index
  before_action :set_find_user, only: [:show, :edit, :update, :destroy]
  before_action :check_group, only: [:new, :create]
  before_action :set_breadcrumb
  before_action :set_options_for_select, only: [:new, :update, :show]
  layout('application')

  # GET /find_users
  def index
    @breadcrumbs_subtitle = 'Listado de Búsqueda'
    @country_options = Country.all.map {|country| [country.name, country.id]}
    @game_options = Game.all.map { |game| [game.name, game.id] }.sort
    @tipo_equipo = [['Teams', 'amateur'],['Teams Pro', 'pro']]

    if params[:nombre].nil?
      find_users = FindUser.all

      if params[:filter].present?
        find_users_all = FindUser.all
        params_select = params.select {|k,v| [k.to_sym,v] if v.present? && k != 'controller' && k != 'filter' && k != 'action' && k != 'commit'}
        search_params = params_select.to_enum.each {|param| ["#{param.first} LIKE ?", param.last]}

        find_users = find_users_all.where(search_params.permit(:nombre, :winrate, :partidas_jugadas, :rango_horario, :pais, :juego, :rol, :posicion, :tipo))
      end
    else
      find_groups = Group.where(Group.arel_table[:name].matches('%'+params[:nombre]+'%'))
      find_users = FindUser.where(group_id: find_groups.ids)
    end

    @find_users_pagination = find_users.paginate(page: params[:page], per_page: 6).order(created_at: :asc)
  end

  # GET /find_users/1
  def show
  end

  # GET /find_users/new
  def new
    @find_user = FindUser.new
    @breadcrumbs_subtitle = 'Nueva Búsqueda'
  end

  # GET /find_users/1/edit
  def edit
    @breadcrumbs_subtitle = 'Modificar Búsqueda'
    @games = Game.all.map { |game| [game.name, game.id] }.sort
    @countries = Country.all.map {|country| [country.name, country.id]}
  end

  # POST /find_users
  def create
    @find_user = FindUser.new(find_user_params)
    @find_user.user_id = current_user.id
    @find_user.group_id = Group.find_by(user_id: current_user.id).id
    @find_user.tipo = current_user.tipo

    if @find_user.save
      redirect_to find_users_path, notice: 'Búsqueda de jugador creada.'
    else
      render :new
    end
  end

  # PATCH/PUT /find_users/1
  def update
    @find_user.tipo = User.find_by(id: @find_user.user_id).tipo
    if @find_user.update(find_user_params)
      redirect_to find_users_path, notice: 'Búsqueda de jugador actualizada.'
    else
      render :edit
    end
  end

  # DELETE /find_users/1
  def destroy
    @find_user.destroy
    redirect_to find_users_url, notice: 'Búsqueda de jugador eliminada.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_find_user
      @find_user = FindUser.find(params[:id])
    end

    def set_options_for_select
      @games = Game.all.map { |game| [game.name, game.id] }
      @countries = Country.all.map { |country| [country.name, country.id] }
    end

    def set_breadcrumb
      @breadcrumbs = 'Equipos buscando jugadores'
      @find_active = 'active'
    end

    def check_group
      redirect_to user_path(current_user.id), notice: 'No tienes un equipo creado' unless Group.find_by(user_id: current_user.id).present?
    end

    # Only allow a list of trusted parameters through.
    def find_user_params
      params.require(:find_user).permit(:user_id, :group_id, :title, :description, :rango_horario, :winrate, :rol, :partidas_jugadas, :posicion, :pais, :juego, :tipo)
    end
end
