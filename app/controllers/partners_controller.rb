class PartnersController < ApplicationController
  before_action :authenticate_user!
  before_action :check_admin
  before_action :set_breadcrumb
  before_action :set_partner, only: [:show, :edit, :update, :destroy]

  # GET /partners
  def index
    @partners = Partner.all
  end

  # GET /partners/1
  def show
  end

  # GET /partners/new
  def new
    @partner = Partner.new
    @breadcrumbs_subtitle = 'Nuevo'
  end

  # GET /partners/1/edit
  def edit
  end

  # POST /partners
  def create
    @partner = Partner.new(partner_params)

    if @partner.save
      redirect_to @partner, notice: 'Partner creado.'
    else
      render :new
    end
  end

  # PATCH/PUT /partners/1
  def update
    if @partner.update(partner_params)
      redirect_to @partner, notice: 'Partner actualizado.'
    else
      render :edit
    end
  end

  # DELETE /partners/1
  def destroy
    @partner.image.purge
    @partner.destroy
    redirect_to partners_url, notice: 'Partner eliminado.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_partner
      @partner = Partner.find(params[:id])
    end

    def check_admin
      redirect_to home_url, notice: 'No estas autorizado para hacer esto' unless current_user.role.id == 2
    end

    def set_breadcrumb
      @breadcrumbs = 'Partners'
    end

    # Only allow a list of trusted parameters through.
    def partner_params
      params.require(:partner).permit(:name, :url, :active, :image)
    end
end
