class AboutsController < ApplicationController
  before_action :authenticate_user!, except: :index
  before_action :set_about, only: %i[ show edit update destroy ]
  before_action :set_breadcrumb
  before_action :check_admin, except: [:index]
  layout('application')

  # GET /abouts or /abouts.json
  def index
    @breadcrumbs_subtitle = 'Free Agents'
    @breadcrumbs_new = new_about_path
    @abouts = About.all.order(created_at: :asc)

    if user_signed_in? && current_user.role.id == 2
      @button = true
    end
  end

  # GET /abouts/1 or /abouts/1.json
  def show
  end

  # GET /abouts/new
  def new
    @breadcrumbs_subtitle = 'Nuevo texto about'
    @about = About.new
  end

  # GET /abouts/1/edit
  def edit
  end

  # POST /abouts or /abouts.json
  def create
    @about = About.new(about_params)

    respond_to do |format|
      if @about.save
        format.html { redirect_to abouts_path, notice: "Texto about guardado." }
        format.json { render :show, status: :created, location: abouts_path }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @about.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /abouts/1 or /abouts/1.json
  def update
    respond_to do |format|
      if @about.update(about_params)
        format.html { redirect_to abouts_path, notice: "Texto about actualizado." }
        format.json { render :show, status: :ok, location: abouts_path }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @about.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /abouts/1 or /abouts/1.json
  def destroy
    @about.destroy
    respond_to do |format|
      format.html { redirect_to abouts_url, notice: "Texto about eliminado." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_about
      @about = About.find(params[:id])
      @button = false
    end

    def set_breadcrumb
      @breadcrumbs = 'About us'
      @free_agents_active = 'active'
    end

    def check_admin
      redirect_to home_path, notice: 'No estas autorizado para hacer esto' unless current_user.role.id == 2
    end

    # Only allow a list of trusted parameters through.
    def about_params
      params.require(:about).permit(:content)
    end
end
