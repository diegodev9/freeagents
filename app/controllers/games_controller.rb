class GamesController < ApplicationController
  before_action :authenticate_user!, except: :index
  before_action :set_game, only: %i[ show edit update destroy ]
  before_action :check_admin, except: [:index, :show]
  before_action :set_breadcrumb
  layout('application')

  # GET /games or /games.json
  def index
    if user_signed_in? && current_user.role.id == 2
      @button = true
    end
    @breadcrumbs_subtitle = 'Juegos'
    #@breadcrumbs_new = new_game_path
    @active_users = User.where(active: true, option: 'jugador')
    @users = @active_users.select {|user| user unless user.games.nil?}

    if params[:nombre].nil?
      games = Game.all
    else
      games = Game.where(Game.arel_table[:name].matches('%'+params[:nombre]+'%'))
    end

    @games_pagination = games.paginate(page: params[:page], per_page: 24).order(name: :asc)
  end

  # GET /games/1 or /games/1.json
  def show
    @breadcrumbs_subtitle = 'Jugadores que juegan a ' + @game.name
    active_users = User.where(active: true, option: 'jugador')
    users_with_games = active_users.select {|user| user unless user.games.nil?}
    get_users = users_with_games.map{|user| user if user.games.scan(/\d*\d/).include?(@game.id.to_s)}.compact
    @country_options = Country.all.map {|country| [country.name, country.id]}
    @game_options = Game.all.map { |game| [game.name, game.id] }.sort

    if params[:nombre].present?
      @breadcrumbs_subtitle = 'Resultados Búsqueda de Equipos'
      @users = get_users.map{|user| user if user.nickname.downcase.include?(params[:nombre].downcase)}.compact
    elsif params[:filter].present?
      @breadcrumbs_subtitle = 'Resultados del filtro'
      params_select = params.select { |k, v| [k.to_sym, v] if v.present? && k != 'controller' && k != 'filter' && k != 'action' && k != 'commit' }
      search_params = params_select.to_enum.each { |param| ["#{param.first} LIKE ?", param.last] }
      @users = User.where(User.arel_table[:games].matches('%' + params[:id] + '%')).and(User.where(search_params.permit(:nombre, :timerange, :gamerole, :country, :winrate)))
      if params[:gameplayed].present?
        @users = @users.map {|user| user if user.games.scan(/\d*\d/).include?(params[:gameplayed])}.compact
      end
    else
      @users = get_users
    end

  end

  # GET /games/new
  def new
    @breadcrumbs_subtitle = 'Nuevo juego'
    @game = Game.new
  end

  # GET /games/1/edit
  def edit
    @breadcrumbs_subtitle = 'Modificar juego'
  end

  # POST /games or /games.json
  def create
    @game = Game.new(game_params)

    respond_to do |format|
      if @game.save
        format.html { redirect_to administrator_index_path(games: 'list'), notice: "Nuevo juego guardado." }
        format.json { render :show, status: :created, location: @game }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /games/1 or /games/1.json
  def update
    respond_to do |format|
      if @game.update(game_params)
        format.html { redirect_to @game, notice: "Juego actualizado." }
        format.json { render :show, status: :ok, location: @game }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /games/1 or /games/1.json
  def destroy
    @users = User.all
    @groups = Group.all
    @users.each do |user|
      unless user.games.nil?
        @user_games = user.games.scan(/\d*\d/) - [params[:id]]
        user.games = @user_games
        user.save
      end
    end
    @groups.each do |group|
      unless group.games.nil?
        @group_games = group.games.scan(/\d*\d/) - [params[:id]]
        group.games = @group_games
        group.save
      end
    end
    @game.destroy
    if params[:games].present?
      redirect_to administrator_index_path(games: 'list')
    else
      redirect_to games_url
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_game
      @game = Game.find(params[:id])
      @button = false
    end

    def set_breadcrumb
      @breadcrumbs = 'Listado de juegos'
      @games_active = 'active'
    end

    def check_admin
      redirect_to home_path, notice: 'No estas autorizado para hacer esto' unless current_user.role.id == 2
    end

    # Only allow a list of trusted parameters through.
    def game_params
      params.require(:game).permit(:name, :avatar, :avatar_big)
    end
end
