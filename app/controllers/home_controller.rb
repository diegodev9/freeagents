class HomeController < ApplicationController
  before_action :set_videos
  before_action :top_videos
  layout 'application'

  def index
    @banners = Banner.where(active: true)
    @top4 = User.all.map {|user| [user.trophy, user.id]}.max(4)
    if user_signed_in?
      @user_own_group = Group.where(user_id: current_user.id)

      mail_count = current_user.mail_for_complete_profile_count.nil? ? 0 : current_user.mail_for_complete_profile_count
      is_profile_complete = current_user.attributes.map {|att| att.last == nil || att.last == ""}.count(true)
      last_mail_sent = current_user.mail_for_complete_profile
      if mail_count <=3 && is_profile_complete != 0
        if last_mail_sent.nil?
          send_mail(mail_count, current_user.id)
        else
          if last_mail_sent + 24*(2**mail_count).hours == Date.today
            send_mail(mail_count, current_user.id)
          end
        end
      end
    end
  end

  def sponsors
  end

  def list_user_equipos
    @breadcrumbs = 'Listado de Equipos'
    @breadcrumbs_subtitle = 'Equipos'
    @users = User.where(option: 'equipo')
    @users_pagination = @users.paginate(page: params[:page], per_page: 6).order(nickname: :asc)
  end

  def firsttimeuser
    @user = User.find_by(id: current_user.id)
    @user.update(firsttime: 0)
    redirect_to user_path(@user)
  end

  def ggwp
    @ggwp_active = 'active'
    @videos_pagination = @videos.paginate(page: params[:page], per_page: 6)
    def find_video(video)
      @video = Video.find_by id: video
      @user = User.find_by id: @video.user_id
    end
  end

  def ggwp_all
    @videos_pagination = @videos.paginate(page: params[:page], per_page: 24)
    def find_video(video)
      @video = Video.find_by id: video
      @user = User.find_by id: @video.user_id
    end
  end

  def terms_conditions
    @breadcrumbs = 'Legales'
    @breadcrumbs_subtitle = 'Términos y condiciones'
  end

  private
  def top_videos
    start_month = Date.today.last_month.beginning_of_month
    end_month = Date.today.last_month.end_of_month
    start_week = Date.today.last_week.beginning_of_week
    end_week = Date.today.last_week.end_of_week
    #@videos_week = Video.select { |video| video.id if video.created_at.year == Date.today.year && video.created_at.month == Date.today.month && video.created_at.to_date.cweek == Date.today.cweek && video.approved }
    @videos_week = Video.select { |video| video.id if video.created_at.to_date.between?(start_week, end_week) && video.approved }
    #@videos_month = Video.select { |video| video.id if video.created_at.year == Date.today.year && video.created_at.month == Date.today.month && video.approved}
    @videos_month = Video.select { |video| video.id if video.created_at.to_date.between?(start_month, end_month) && video.approved}
    @top3 = @videos_month.map {|video| [Vote.where(video_id: video).count, video]}.max(3)
    @top1 = @videos_month.map {|video| [Vote.where(video_id: video).count, video]}.max
    @top_week = @videos_week.map {|video| [Vote.where(video_id: video).count, video]}.max
    @videos_all = Video.all.map {|video| video.id}
    @top3_all = @videos_all.map {|video| [Vote.where(video_id: video).count, video]}.max(3)
  end

  def send_mail(mail_count, user_id)
    @sendmail = Contact.new(
      nombre: 'FreeAgents',
      email: 'info@freeagents.gg',
      nickname: '',
      mail_to: current_user.email,
      asunto: 'Completa tu perfil de Freeagents',
      mensaje: "Hola!, #{current_user.name}. Te enviamos este mensaje para recordarte que necesitamos que completes tu perfil de FreeAgents. Gracias!",
      opcion: 'completar perfil'
    )
    @sendmail.request = request
    @sendmail.deliver
    user = User.find_by(id: user_id)
    user.update(mail_for_complete_profile: Date.today, mail_for_complete_profile_count: mail_count + 1)
  end

  def set_videos
    @videos = Video.where(approved: true)
  end
end