class Faq < ApplicationRecord
  validates :faq_title, presence: true
  validates :faq_content, presence: true
end
