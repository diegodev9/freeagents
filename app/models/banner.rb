class Banner < ApplicationRecord
  has_one_attached :image, dependent: :destroy
  has_one_attached :thumb, dependent: :destroy

  validates :title, uniqueness: true
end
