class Video < ApplicationRecord
  validates :video, uniqueness: true
  belongs_to :user
  has_many :votes, dependent: :destroy

  before_save :assign_not_approved

  def assign_not_approved
    self.approved = false if approved.nil?
  end

  def voted?(user)
    !!self.votes.find{ |vote| vote.user_id == user.id }
  end
end
