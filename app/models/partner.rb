class Partner < ApplicationRecord
  has_one_attached :image, dependent: :destroy
  validate :acceptable_image

  def acceptable_image
    return unless image.attached?

    unless image.byte_size <= 1.megabyte
      errors.add(:image, "El archivo no debe exceder 1MB")
    end

    acceptable_types = ["image/jpeg", "image/png", "image/jpg"]

    unless acceptable_types.include?(image.content_type)
      errors.add(:image, "La imagen debe ser .jpg, .jpeg o .png")
    end
  end
end
