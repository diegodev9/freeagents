class Group < ApplicationRecord
  belongs_to :user
  has_one_attached :image, dependent: :destroy
  has_many :group_invitations, dependent: :destroy
  has_many :find_users, dependent: :destroy

  validates :name, uniqueness: true
  validate :acceptable_image

  before_save :check_league

  def acceptable_image
    return unless image.attached?

    unless image.byte_size <= 1.megabyte
      errors.add(:image, "El archivo no debe exceder 1MB")
    end

    acceptable_types = ["image/jpeg", "image/png", "image/jpg"]

    unless acceptable_types.include?(image.content_type)
      errors.add(:image, "La imagen debe ser .jpg, .jpeg o .png")
    end
  end

  def check_league
    if self.league.nil? || self.league.empty?
      self.league = 'Sin liga'
    end
  end
end
