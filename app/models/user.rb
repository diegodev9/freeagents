class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :confirmable

  belongs_to :role, optional: true
  has_many :videos, dependent: :destroy
  has_many :votes, dependent: :destroy
  has_many :group_invitations, dependent: :destroy
  has_many :find_users, dependent: :destroy
  has_many :find_teams, dependent: :destroy
  has_many :groups, dependent: :destroy
  has_one_attached :image, dependent: :destroy

  validates :nickname, presence: true, uniqueness: true
  validates :email, presence: true, uniqueness: true
  validates :whatsapp, presence: true, numericality: { only_integer: true }, on: :create
  validate :acceptable_image
  before_create :check_age, :check_tipo
  before_save { self.email = email.downcase }
  before_save :assign_role, :check_firsttime
  before_save { self.trophy = 0 if trophy.nil? }

  def assign_role
    self.role = Role.find_by name: 'jugador' if role.nil?
  end

  def check_firsttime
    self.firsttime = 1 if firsttime.nil?
  end

  def check_tipo
    self.tipo = 'amateur'
  end

  def check_age
    user_age = ((Time.zone.now - self.borndate.to_time) / 1.year.seconds).floor
    if user_age < 18
      self.menor = true
    else
      self.active = true
    end
  end

  def acceptable_image
    return unless image.attached?

    unless image.byte_size <= 1.megabyte
      errors.add(:image, "El archivo no debe exceder 1MB")
    end

    acceptable_types = ["image/jpeg", "image/png", "image/jpg"]

    unless acceptable_types.include?(image.content_type)
      errors.add(:image, "La imagen debe ser .jpg, .jpeg o .png")
    end
  end
end
