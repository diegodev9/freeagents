class Contact < MailForm::Base
  attribute :nombre, :validate => true
  attribute :email,     :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attribute :mensaje
  attribute :nickname,  :captcha  => true
  attribute :asunto
  attribute :mail_to
  attribute :logo, attachment: true
  attribute :background, attachment: true
  attribute :opcion

  def headers
    {
      subject: asunto,
      to: mail_to,
      from: %("#{nombre}" <#{email}>)
    }
  end
end
