class Game < ApplicationRecord
  has_one_attached :avatar, dependent: :destroy
  has_one_attached :avatar_big, dependent: :destroy

  validates :name, presence: true
  validate :acceptable_image

  def acceptable_image
    return unless avatar.attached?

    unless avatar.byte_size <= 1.megabyte
      errors.add(:avatar, "El archivo no debe exceder 1MB")
    end

    acceptable_types = ["image/jpeg", "image/png", "image/jpg"]

    unless acceptable_types.include?(avatar.content_type)
      errors.add(:avatar, "La imagen debe ser .jpg, .jpeg o .png")
    end
  end
end
