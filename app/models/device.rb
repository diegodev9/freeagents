class Device < ApplicationRecord
  validates :name, uniqueness: true
end
