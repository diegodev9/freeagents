module ApplicationHelper
  def find_games
    @games = Game.all.order(name: :asc)
  end

  def find_devices
    @devices = Device.all.order(name: :asc)
  end

  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def find_video(video)
    @video = Video.find_by id: video
    @user = User.find_by id: @video.user_id
  end
end
